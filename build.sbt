name := "approvals-scala"

crossScalaVersions := List("2.12.20", "2.13.16", "3.6.4")

libraryDependencies ++= Seq(scalaTest, approvalsJava, pprint)

resolvers += Resolver.mavenLocal

lazy val pprint        = "com.lihaoyi"   %% "pprint"         % "0.9.0"
lazy val scalaTest     = "org.scalatest" %% "scalatest"      % "3.2.19"
lazy val approvalsJava = "com.colisweb"   % "approvals-java" % "0.13.4"
ThisBuild / pushRemoteCacheTo := Some(
  MavenCache("local-cache", baseDirectory.value / sys.env.getOrElse("CACHE_PATH", "sbt-cache"))
)

Global / onChangedBuildSource := ReloadOnSourceChanges
